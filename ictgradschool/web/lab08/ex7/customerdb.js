var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];
window.onload = function() {
    var col = [];
    for (var i = 0; i < customers.length; i++) {
        for (var key in customers[i]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    var table = document.createElement("table");

    var tr = table.insertRow(-1);

    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");
        th.innerHTML = col[i];
        tr.appendChild(th);
    }


    for (var i = 0; i < customers.length; i++) {

        tr = table.insertRow(-1);

        for (var j = 0; j < col.length; j++) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = customers[i][col[j]];
        }
    }


    var divContainer = document.getElementById("example-table");
    divContainer.appendChild(table);


    var summary = document.createElement("table");

    var statCol = ["Total Males", "Total Females", "Aged 0-30", "Aged 31-64", "Aged 65+", "Gold", "Silver", "Bronze"];

    console.log(statCol);

    var tr2 = summary.insertRow(-1);

    for (var i = 0; i < statCol.length; i++) {
        var th2 = document.createElement("th");
        th2.innerHTML = statCol[i];
        tr2.appendChild(th2);
    }
    tr2.insertCell(-1);


    var divContainer2 = document.getElementById("summary-table");
    divContainer2.appendChild(summary);

    var totalMales = 0;
    var totalFemales = 0;
    var age0_30 = 0;
    var age31_64 = 0;
    var age65 = 0;
    var gold = 0;
    var silver = 0;
    var bronze = 0;

    for (var i = 0; i < customers.length; i++) {
        if (customers[i].gender === "male") {
            totalMales++;
        } else {
            totalFemales++;
        }
        if (customers[i].year_born >= 1988) {
            age0_30++;
        } else if (customers[i].year_born < 1988 && customers[i].year_born >= 1954) {
            age31_64++;
        } else {
            age65++;
        }
        var loyalty = customers[i].num_hires / ((2018 - customers[i].joined) * 52);
        if (loyalty > 4) {
            gold++;
        } else if (loyalty < 4 && loyalty > 1) {
            silver++;
        } else {
            bronze++;
        }

    }
    var stats = [totalMales, totalFemales, age0_30, age31_64, age65, gold, silver, bronze];

    var tr3 = summary.insertRow(-1);
    for (var i = 0; i < stats.length; i++) {
        var statCell = tr3.insertCell(-1);
        statCell.innerHTML = stats[i];
    }
};