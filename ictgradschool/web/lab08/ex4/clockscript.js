var timer = null;

var startTime = function() {
    var time = new Date();
    document.getElementById("initial").innerText = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
};

window.onload = function() {
    startTime();
    timer = setInterval(startTime, 1000);
    var stopTimeButton = document.getElementById("stop");
    stopTimeButton.onclick = function () {

        clearInterval(timer);
    };
    var restartTimeButton = document.getElementById("restart");
    restartTimeButton.onclick = function (){
        timer = setInterval(startTime, 1000);
    }
};





